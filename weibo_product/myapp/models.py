from django.db import models

# Create your models here.


class RMRBKeyWordsModel(models.Model):
    keyword = models.CharField(verbose_name = '关键字', max_length = 255, unique = True)
    weight_count = models.IntegerField(verbose_name = '权重')

    class Meta:
        verbose_name = '人民日报关键字'
        verbose_name_plural = verbose_name


class RMRBModel(models.Model):
    title = models.CharField(verbose_name = '标题', max_length = 512)
    content = models.TextField(verbose_name = '内容')
    link = models.URLField(verbose_name = '链接', max_length = 255)
    send_count = models.IntegerField(verbose_name = '转发数')

    class Meta:
        verbose_name = '人民日报微博'
        verbose_name_plural = verbose_name


class YouthKeyWordsModel(models.Model):
    keyword = models.CharField(verbose_name = '关键字', max_length = 255, unique = True)
    weight_count = models.IntegerField(verbose_name = '权重')

    class Meta:
        verbose_name = '中国青年关键字'
        verbose_name_plural = verbose_name


class YouthModel(models.Model):
    title = models.CharField(verbose_name = '标题', max_length = 512)
    content = models.TextField(verbose_name = '内容')
    link = models.URLField(verbose_name = '链接', max_length = 255)
    send_count = models.IntegerField(verbose_name = '转发数')

    class Meta:
        verbose_name = '中国青年微博'
        verbose_name_plural = verbose_name


class NewsKeyWordsModel(models.Model):
    keyword = models.CharField(verbose_name = '关键字', max_length = 255, unique = True)
    weight_count = models.IntegerField(verbose_name = '权重')

    class Meta:
        verbose_name = '新闻周刊关键字'
        verbose_name_plural = verbose_name


class NewsModel(models.Model):
    title = models.CharField(verbose_name = '标题', max_length = 512)
    content = models.TextField(verbose_name = '内容')
    link = models.URLField(verbose_name = '链接', max_length = 255)
    send_count = models.IntegerField(verbose_name = '转发数')

    class Meta:
        verbose_name = '新闻周刊微博'
        verbose_name_plural = verbose_name


class HeadNewsKeyWordsModel(models.Model):
    keyword = models.CharField(verbose_name = '关键字', max_length = 255, unique = True)
    weight_count = models.IntegerField(verbose_name = '权重')

    class Meta:
        verbose_name = '头条新闻关键字'
        verbose_name_plural = verbose_name


class HeadNewsModel(models.Model):
    title = models.CharField(verbose_name = '标题', max_length = 512)
    content = models.TextField(verbose_name = '内容')
    link = models.URLField(verbose_name = '链接', max_length = 255)
    send_count = models.IntegerField(verbose_name = '转发数')

    class Meta:
        verbose_name = '头条新闻微博'
        verbose_name_plural = verbose_name


class RMRBFansModel(models.Model):
    nickname = models.CharField(verbose_name = '昵称', max_length = 255, unique = True)
    location = models.CharField(verbose_name = '地区', max_length = 255)
    sex = models.CharField(verbose_name = '性别', max_length = 255)