#!/usr/bin/env python
# -*- coding: utf-8 -*-
import traceback
import django
import os
import jieba
from jieba import analyse
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "weibo_web.settings")
django.setup()
from myapp.models import *
__version__ = '1.0.0.0'

"""
@brief 简介 
@details 详细信息
@author  zhphuang
@data    2018-04-21 
"""
# -*- coding: utf-8 -*-

import os
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium import webdriver
from bs4 import BeautifulSoup
from collections import Counter
import sys
import time
import re
import requests


class Spider(object):
    """
    爬取类
    """
    def __init__(self):
        options = webdriver.ChromeOptions()
        options.add_experimental_option("excludeSwitches", ["ignore-certificate-errors"])
        # self.browser = webdriver.PhantomJS(executable_path=self._getdriverpath())
        self.browser = webdriver.Chrome(self._getdriverpath(), chrome_options=options)
        self.browser.implicitly_wait(60) # 隐性等待，最长等30秒
        self.browser.maximize_window()

    def get_data(self):
        current_page = 1
        self.browser.get("%s?page=%s" % (self.url, current_page))
        flag = True
        while flag:
            bs = BeautifulSoup(self.browser.page_source, "html.parser")
            div_list = bs.select("div.c")
            for item in div_list:
                try:
                    time_span = item.select("div span.ct")
                    if time_span:
                        if time_span[0].text.find("01月") != -1:
                            flag = False
                    else:
                        continue
                    span = item.select("div span.ctt")[0]
                    obj = self.model_name()
                    text = span.text
                    group = re.search(r'【(.*?)】(.*)', text)
                    if not group:
                        continue
                    obj.title = group.group(1)
                    obj.content = group.group(2)
                    self.analyze(obj.title+"," + obj.content)
                    for item2 in item.select("div a"):
                        if item2.attrs['href'].find("https://weibo.cn/repost/") != -1:
                            obj.send_count = int(item2.text.split("[")[1][:-1])
                            break
                    print("title:%s\ncontent:%s\nsend_count:%s timespan:%s\n" % (obj.title, obj.content, obj.send_count, time_span[0].text))
                    print("#"*50)
                    obj.save()
                except Exception as e:
                    print(traceback.format_exc())
                    print(e)
            page = self.browser.find_element_by_css_selector("#pagelist>form>div>a")
            page.click()
            time.sleep(2)
        self.browser.close()

    def quit(self):
        self.browser.close()

    def analyze(self, description):
        jieba.analyse.set_stop_words('stop_words.txt')
        stop_dict = {}
        content = open("stop_words.txt", "rb").read().decode('utf-8')
        for wd in content.split('\n'):
            stop_dict[wd] = 1
        tk = jieba.cut(description, cut_all=True)
        data = dict(Counter(tk))

        pat = re.compile(r'\d')
        keywords = {}
        for key, value in data.items():
            if key and not pat.search(key) and key not in stop_dict and len(key)>=2:
                if key in keywords:
                    keywords[key] += value
                else:
                    keywords[key] = value
        return keywords

    def _getdriverpath(self):
        path = "C://chromedriver.exe"
        return path


class SpiderRMRB(Spider):
    def __init__(self):
        super(SpiderRMRB, self).__init__()
        self.url = "https://weibo.cn/rmrb"
        self.browser.get(self.url)
        time.sleep(20)
        self.model_name = RMRBModel

    def analyze(self, description):
        keywords = super(SpiderRMRB, self).analyze(description)
        print("keyword:%s\n" % keywords)
        for key, counter in keywords.items():
            obj, is_create = RMRBKeyWordsModel.objects.get_or_create(keyword=key, defaults = {"weight_count": counter})
            if not is_create:
                obj.weight_count += counter
                obj.save()


class SpiderYonth(Spider):
    def __init__(self):
        super(SpiderYonth, self).__init__()
        self.url = "https://weibo.cn/bjyouth"
        self.browser.get(self.url)
        time.sleep(20)
        self.model_name = YouthModel

    def analyze(self, description):
        keywords = super(SpiderYonth, self).analyze(description)
        print("keyword:%s\n" % keywords)
        for key, counter in keywords.items():
            obj, is_create = YouthKeyWordsModel.objects.get_or_create(keyword=key, defaults = {"weight_count": counter})
            if not is_create:
                obj.weight_count += counter
                obj.save()


class SpiderNews(Spider):
    def __init__(self):
        super(SpiderNews, self).__init__()
        self.url = "https://weibo.cn/chinanewsweek"
        self.browser.get(self.url)
        time.sleep(20)
        self.model_name = NewsModel

    def analyze(self, description):
        keywords = super(SpiderNews, self).analyze(description)
        print("keyword:%s\n" % keywords)
        for key, counter in keywords.items():
            obj, is_create = NewsKeyWordsModel.objects.get_or_create(keyword=key, defaults = {"weight_count": counter})
            if not is_create:
                obj.weight_count += counter
                obj.save()


class SpiderHeadNews(Spider):
    def __init__(self):
        super(SpiderHeadNews, self).__init__()
        self.url = "https://weibo.cn/breakingnews"
        self.browser.get(self.url)
        time.sleep(20)
        self.model_name = HeadNewsModel

    def analyze(self, description):
        keywords = super(SpiderHeadNews, self).analyze(description)
        print("keyword:%s\n" % keywords)
        for key, counter in keywords.items():
            obj, is_create = HeadNewsKeyWordsModel.objects.get_or_create(keyword=key, defaults = {"weight_count": counter})
            if not is_create:
                obj.weight_count += counter
                obj.save()


class SpiderRMRBFans(Spider):
    def __init__(self):
        super(SpiderRMRBFans, self).__init__()
        self.url = "https://weibo.cn/2803301701/fans"
        self.browser.get(self.url)
        time.sleep(20)

        self.cookie = {}
        for item2 in self.browser.get_cookies():
            self.cookie[item2["name"]] = item2["value"]

    def get_data(self):
        current_page = 1
        while current_page < 20:
            self.browser.get("%s?page=%s" % (self.url, current_page))
            bs = BeautifulSoup(self.browser.page_source, "html.parser")
            table_list = bs.select("div.c table tbody")
            for item in table_list:
                try:
                    href = item.select("tr td")[0].select("a")[0].attrs["href"]
                    group = re.search(r'https://weibo.cn/u/(.*)', href)
                    fan_id = group.group(1)
                    href = "https://weibo.cn/%s/info" % fan_id
                    headers = {
                            "Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
                            "Accept-Encoding":"gzip, deflate, sdch, br",
                            "Accept-Language":"zh-CN,zh;q=0.8",
                            "Referer":href,
                            "User-Agent":"Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36"
                    }
                    res = requests.get(href, headers=headers, cookies = self.cookie)
                    bs2 = BeautifulSoup(res.text, "html.parser")
                    info = bs2.select("body div.c")[3].text
                    print(info)
                    group = re.search(r'昵称:(.*?)性别:(.*?)地区:(.*)', info)
                    print(info)
                    obj = RMRBFansModel()
                    obj.nickname = group.group(1)
                    obj.sex = group.group(2)
                    obj.location = group.group(3)[:5]
                    obj.save()
                except Exception as e:
                    print(traceback.format_exc())
                    print(e)
            current_page += 1



if __name__ == '__main__':
    # SpiderRMRB().get_data()
    # SpiderYonth().get_data()
    # SpiderNews().get_data()
    # SpiderHeadNews().get_data()
    a = SpiderRMRBFans()
    i = 0
    while i < 10:
        a.get_data()
        i += 1
    a.browser.close()